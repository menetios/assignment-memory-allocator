#include "mem.h"
#define _GNU_SOURCE
#include "mem_internals.h"



void free_heap (void *heap){
    struct block_header* header = heap;
    while(header)
    {
        _free(header->contents);
        header = header->next;
    }
    _free(((struct block_header*) heap)->contents);
}




int main(){
    
    void* heap = heap_init(20000);

    //success
    fprintf(stdout, "\nTest 1\n");
     _malloc(100);
    _malloc(100);
    _malloc(200);
    _malloc(300);
    debug_heap(stdout, heap);
    free_heap(heap);

    //free one block
    fprintf(stdout, "\nTest 2\n");
    _malloc(200);
    _malloc(1000);
    _malloc(20);
    void* a = _malloc(789);
    _malloc(123);
    _free(a);
    debug_heap(stdout, heap);
    free_heap(heap);

    //free two blocks
    fprintf(stdout, "\nTest 3\n");
    _malloc(200);
    void* b = _malloc(1000);
    _malloc(20);
    void* c= _malloc(789);
    _malloc(123);
    _free(c);
    _free(b);
    debug_heap(stdout, heap);
    free_heap(heap);

    //extend by new region
    fprintf(stdout, "\nTest 4\n");
    _malloc(21000);
    _malloc(2000);
    debug_heap(stdout, heap);
    free_heap(heap);

    //extend fail
    fprintf(stdout, "\nTest #5\n");
    struct block_header* header = heap;
    void* p = (void*) ((uint8_t *)header + size_from_capacity(header->capacity).bytes);
    mmap(p, 24562, PROT_READ | PROT_WRITE, MAP_PRIVATE |  MAP_ANONYMOUS |  MAP_FIXED, 0, 0);
    void* temp = _malloc(66666);

    if (temp == NULL) {
        fprintf(stdout, "Temp is null");
    }

    _free(temp);
    debug_heap(stdout, heap);
    free_heap(heap);

    return 0;
}
